const quizDataBeginner = `{
  "title": "Questions",
  "questions": [
    {
      "text": "What is 2+15?",
      "type": "mc",
      "answers": [
        "17",
        "18",
        "20",
        "22"
      ],
      "answer": "17"
    },
    {
      "text": "Calculate 3*2 ",
      "type": "mc",
      "answers": [
        "5",
        "6",
        "7",
        "8"
      ],
      "answer": "6"
    },
    {
      "text": "Calculate 13*2 ",
      "type": "mc",
      "answers": [
        "25",
        "26",
        "27",
        "28"
      ],
      "answer": "26"
    },
    {
      "text": "Calculate 32/2 ",
      "type": "mc",
      "answers": [
        "16",
        "12",
        "10",
        "11"
      ],
      "answer": "16"
    }
  ]
}`;

const quizDataIntermediate = `{
  "title": "Questions",
  "questions": [
    {
      "text": "What is 0/2?",
      "type": "mc",
      "answers": [
        "1",
        "28",
        "10",
        "0"
      ],
      "answer": "0"
    },
    {
      "text": "Calculate 10/1 ",
      "type": "mc",
      "answers": [
        "5",
        "6",
        "7",
        "10"
      ],
      "answer": "10"
    },
    {
      "text": "Calculate 2+3+4 ",
      "type": "mc",
      "answers": [
        "5",
        "6",
        "7",
        "9"
      ],
      "answer": "9"
    }
  ]
}`;

const quizDataAdvance = `{
  "title": "Questions",
  "questions": [
    {
      "text": "What is 2+15-4-2?",
      "type": "mc",
      "answers": [
        "17",
        "18",
        "20",
        "11"
      ],
      "answer": "11"
    },
    {
      "text": "Calculate 3+2+3 ",
      "type": "mc",
      "answers": [
        "5",
        "6",
        "7",
        "8"
      ],
      "answer": "8"
    },
    {
      "text": "Calculate 3*2*2 ",
      "type": "mc",
      "answers": [
        "25",
        "16",
        "27",
        "12"
      ],
      "answer": "12"
    }
  ]
}`;

/* get parameters*/
let uri = window.location.search.substring(1); 
let params = new URLSearchParams(uri);
let testId= params.get("testId");
let title='Welcome to Quiz';

/* set title*/
if(testId==1){
  testName='Math';
}
else if(testId==2){
  testName='Science';
}
else if(testId==3){
  testName='Art';
}
title='Welcome to Quiz of Topic '+testName; 

/*component  ques */
Vue.component('question', {
  template:`
<div class='align-center'>
  <h1>Q {{ questionNumber }} )
  {{ question.text }} </h1>

  <div v-if="question.type === 'mc'">
    <div v-for="(mcanswer,index) in question.answers">
    <input type="radio" :id="'answer'+index" name="currentQuestion" v-model="answer" :value="mcanswer"><label :for="'answer'+index">{{mcanswer}}</label><br/>
    <br>
    </div>
  </div>

  <button class="custom-btn" v-on:click="submitAnswer">Submit</button>
</div>
`,
  data() {
     return {
       answer:''
     }
  },
  props:['question','question-number'],
  methods:{
    submitAnswer:function() {
      this.$emit('answer', {answer:this.answer});
      this.answer = null;
    }
  }
});

/* json get */
const app = new Vue({
  el:'#quiz',
  data() {
    return {
      introStage:false,
      questionStage:false,
      resultsStage:false,
      title:title,
      questions:[],
      currentQuestion:0,
      answers:[],
      correct:0,
      perc:null
    }
  },
  created() {
    let resQuestions=[];
    resBeginner= JSON.parse(quizDataBeginner);
    resIntermediate= JSON.parse(quizDataIntermediate);
    resAdvance= JSON.parse(quizDataAdvance);
    
    resQuestions.push(resBeginner.questions);
    resQuestions.push(resIntermediate.questions);
    resQuestions.push(resAdvance.questions);
    console.log(resQuestions.flat());
    resQuestionsFinal =resQuestions.flat()
      this.questions =resQuestionsFinal;
      this.introStage = true;
   
  },
  methods:{
    startQuiz() {
      this.introStage = false;
      this.questionStage = true;
    },
    handleAnswer(e) {
      this.answers[this.currentQuestion]=e.answer;
      if((this.currentQuestion+1) === this.questions.length) {
        this.handleResults();
        this.questionStage = false;
        this.resultsStage = true;
      } else {
        this.currentQuestion++;
      }
    },
    handleResults() {
      this.questions.forEach((a, index) => {
        if(this.answers[index] === a.answer) this.correct++;        
      });
      this.perc = ((this.correct / this.questions.length)*100).toFixed(2);
    }
  }
})